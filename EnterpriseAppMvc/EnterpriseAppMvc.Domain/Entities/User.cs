﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseAppMvc.Domain.Entities
{
    public enum UserType : byte
    {
        User = 1,
        Manager,
        Admin
    }
    public class User
    {

        public User()
        {

        }

        public int UserID { get; set; }

        [Required (ErrorMessage = "Please provide a username")]
        public string Username { get; set; }

        [Required (ErrorMessage = "Please provide a password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Salt { get; set; }

        public UserType Type { get; set; }

    }
}
