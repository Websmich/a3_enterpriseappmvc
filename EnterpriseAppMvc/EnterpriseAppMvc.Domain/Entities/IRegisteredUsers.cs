﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseAppMvc.Domain.Entities
{
    public interface IRegisteredUsers
    {
        IEnumerable<User> Userlist { get; }

        void AddUser(User user);

        byte[] CreateSaltedPassword(byte[] salt, byte[] plainTextPass);

        User ValidateUser(string username, string password);


        string EncodeString(string unencodedString);
    }
}
