﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseAppMvc.Domain.Persistance;

//  Michael Webster
//  Assignment 3 - Enterprise App with MVC
//  PROG30000 - Enterprise Software Systems
//  November 27th 2016 11:59 PM

namespace EnterpriseAppMvc.Domain.Entities
{
    /// <summary>
    /// The RegisteredInvoices class will handle all of the logic dealing with an invoice, which includes:
    ///     - Containing a list of Invoices
    ///     - Adding Items to this list
    ///     - Load a pre-existing set of invoices from a mock repository
    /// </summary>
    public class RegisteredInvoices : IRegisteredInvoices
    {
        /// <summary>
        /// A field variable which will hold information about the repository
        /// </summary>
        private IInvoiceRepository _repository;


        /// <summary>
        /// Using ninject, the constructor will access an IInvoiceRepository object variable as defined through bindings
        /// in the DomainFactoryModule class
        /// </summary>
        /// <param name="repo"></param>
        public RegisteredInvoices(IInvoiceRepository repo)
        {
            _repository = repo;
        }

        /// <summary>
        /// The InvoiceList will return the list of current invoices in an IEnumerable format for encapsulation pruposes
        /// </summary>
        public IEnumerable<Invoice> InvoiceList
        {
            get
            {
                return _repository.InvoiceDatabase;
            }
        }

        /// <summary>
        /// The RegisterInvoice method will add the newly created to the list of invoices
        /// </summary>
        /// <param name="invoice"></param>
        public void AddInvoice(Invoice invoice)
        {
            _repository.SaveInvoice(invoice);
        }

        /// <summary>
        /// The GetOutstandingInvoiceTotal() method will calculate the total amount of all the orders that have been marked as unpaid (outstanding)
        /// </summary>
        /// <returns></returns>
        public decimal GetOutstandingInvoiceTotal()
        {
            decimal amount = 0;

            foreach(Invoice i in _repository.InvoiceDatabase)
            {
                if (i.Paid)
                {
                    amount += i.InvoiceSubTotal();
                }
            }
            return amount;
        }


        /// <summary>
        /// The FindInvoice method will search through the list of invoices and retireve invoices specific to the search requirements   
        /// </summary>
        /// <param name="unpaidInvoice"></param>
        /// <returns></returns>
        public IEnumerable<Invoice> FindInvoice(Func<Invoice, bool> matchInvoice)
        {
            foreach(Invoice inv in InvoiceList)
            {
                //if the current invoice matches the search parameter, yield return that invoice 
                if (matchInvoice(inv))
                {
                    yield return inv;
                }
            }
        }
    }
}
