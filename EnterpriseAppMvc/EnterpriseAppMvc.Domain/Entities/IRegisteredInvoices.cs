﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//  Michael Webster
//  Assignment 3 - Enterprise App with MVC
//  PROG30000 - Enterprise Software Systems
//  November 27th 2016 11:59 PM

namespace EnterpriseAppMvc.Domain.Entities
{
    public interface IRegisteredInvoices 
    {
        IEnumerable<Invoice> InvoiceList { get; }

        void AddInvoice(Invoice invoice);

        IEnumerable<Invoice> FindInvoice(Func<Invoice, bool> matchInvoice);

        decimal GetOutstandingInvoiceTotal();

    }
}
