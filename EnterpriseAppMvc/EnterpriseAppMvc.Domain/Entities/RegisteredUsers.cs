﻿using EnterpriseAppMvc.Domain.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace EnterpriseAppMvc.Domain.Entities
{
    public class RegisteredUsers : IRegisteredUsers
    {
        /// <summary>
        /// repo object that will allow the communication between this class and the user related database functions
        /// </summary>
        private IUserRepository _repository;

        public RegisteredUsers(IUserRepository repo)
        {
            _repository = repo;
        }
        /// <summary>
        /// contacts the database to retrieve ALL the users
        /// </summary>
        public IEnumerable<User> Userlist
        {
            get
            {
                return _repository.UserDatabase;
            }
        }

        /// <summary>
        /// Method for adding the user to the database
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(User user)
        {
            //encrypt the password using a salt value to hash the password
            PasswordEncryption(user);

            //submit user to the datatbase
            _repository.AddUser(user);
        }

        /// <summary>
        /// Generates a new salt based off the length of the user specified password, and will call upon another clash to hash the password based
        /// off the password and salt
        /// </summary>
        /// <param name="user"></param>
        public void PasswordEncryption(User user)
        {
            string encodedString = EncodeString(user.Password);
            byte[] plainTextPass = Convert.FromBase64String(encodedString);
            //create new byte array of the same length of the password
            byte[] saltValue = new byte[encodedString.Length];
            //using cryptographic services, generate a random salt value
            RNGCryptoServiceProvider randomGen = new RNGCryptoServiceProvider();
            randomGen.GetBytes(saltValue);

            byte[] hashedPassword = CreateSaltedPassword(saltValue, plainTextPass);

            //set the salt used for the user 
            user.Salt = Convert.ToBase64String(saltValue);
            //set the password to now be a hashed value based on the original password and the randomly generated salt
            user.Password = Convert.ToBase64String(hashedPassword);

            
        }

        /// <summary>
        /// Receives the salt value generated from the user created password, and the password itself. The method will combine both the password in plain
        /// text form and the salt and with that, will hash the password
        /// </summary>
        /// <param name="salt"></param>
        /// <param name="plainTextPass"></param>
        /// <returns></returns>
        public byte[] CreateSaltedPassword(byte[] salt, byte[] plainTextPass)
        {
            byte[] rawSalted = new byte[plainTextPass.Length + salt.Length];
            //add password to the to-be salted pass
            plainTextPass.CopyTo(rawSalted, 0);
            // add the salt value after the password. Array is now "{password}+{salt}"
            salt.CopyTo(rawSalted, plainTextPass.Length);

            //hash the password using SHA256
            SHA256 sha = SHA256.Create();
            byte[] saltedPass = sha.ComputeHash(rawSalted);

            byte[] hashedPassword = new byte[saltedPass.Length + salt.Length];
            saltedPass.CopyTo(hashedPassword, 0);
            salt.CopyTo(hashedPassword, saltedPass.Length);

            string saltTest = Encoding.Default.GetString(salt);

            //return hashed password
            return hashedPassword; 
        }

        /// <summary>
        /// Using the username and password provided by the user on the login screen, validate the user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public User ValidateUser(string username, string password)
        {
            User validatingUser = _repository.RetrieveUser(username); //retrieve the details regarding the user based on the username provided

            if (validatingUser != null)
            {
                byte[] encodedUserPass = Convert.FromBase64String(EncodeString(password));
                //hash the password entered on the login screen using the salt value relating to that user
                byte[] enteredPassword = CreateSaltedPassword(Convert.FromBase64String(validatingUser.Salt), encodedUserPass);

                string hashedUserPassword = Convert.ToBase64String(enteredPassword);

                //compare newly hashed password with the one from the database
                if (hashedUserPassword == validatingUser.Password)
                {
                    return validatingUser; //return the user
                }
                else
                {
                    return new User(); //return a user which does not exists (ID of 0)
                }
            }
            else
            {
                return new User();
            }

        }

        /// <summary>
        /// Encodes a string to Base64
        /// </summary>
        /// <param name="unencodedString"></param>
        /// <returns></returns>
        public string EncodeString(string unencodedString)
        {

            var encodedString = Encoding.UTF8.GetBytes(unencodedString);
            return Convert.ToBase64String(encodedString);

        }

    }
}
