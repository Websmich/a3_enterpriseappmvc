﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//  Michael Webster
//  Assignment 3 - Enterprise App with MVC
//  PROG30000 - Enterprise Software Systems
//  November 27th 2016 11:59 PM

namespace EnterpriseAppMvc.Domain.Entities
{
    /// <summary>
    /// The currency enumeration will model different types of currency that are available for payment options to the user
    /// </summary>
    public enum Currency
    {
        CAD = 1,
        USD,
        EUR
    }
    /// <summary>
    /// The invoice class will serve as a model for what an invoice will look like, which will include attributes about
    /// the invoice itself, such as:
    ///     - Name of client who the invoice is addressed to: String
    ///     - The address of the client: String
    ///     - The date of shipment: DateTime
    ///     - The date the payment is due: DateTime
    ///     - The name of the product: String
    ///     - The quantity of the product: int
    ///     - The unit price of EACH product: decimal
    ///     - The currency of the product: Currency
    ///     - Tracking number of the shipment: int
    /// </summary>
    public class Invoice
    {

        /// <summary>
        /// The default constructor to automatically set the invoice to be unpaid 
        /// </summary>
        public Invoice()
        {
            Paid = false;
            InvoiceNumber = 0;
        }

        /// <summary>
        /// The tracking number provided to allow the user to track the invoice
        /// </summary>
        [Key]
        public int InvoiceNumber { get; set; }

        /// <summary>
        /// The name of the client who filed the invoice
        /// </summary>
        [Required(ErrorMessage = "Please provide the name of the client.")]
        public string ClientName { get; set; }

        /// <summary>
        /// The address which the client has provided
        /// </summary>
        [Required(ErrorMessage = "Please provide the address")]
        public string ClientAddress { get; set; }

        /// <summary>
        /// The date of when the ordered product will ship
        /// </summary>

        [Required(ErrorMessage = "Please enter a valid date in the mm-dd-yyyy format")]
        [DataType(DataType.Date)]
        public DateTime ShipmentDate { get; set; }

        /// <summary>
        /// The date when the invoice will be due
        /// </summary>
        [Required(ErrorMessage ="Please enter a valid date in the mm-dd-yyyy format")]
        [DataType(DataType.Date)]
        public DateTime PaymentDueDate { get; set; }

        /// <summary>
        /// The name of the product that the invoice is for
        /// </summary>
        [Required(ErrorMessage ="Please provide the name of the product")]
        public string ProductName { get; set; }

        /// <summary>
        /// The amount of products included in this invoice
        /// </summary>
        [Required(ErrorMessage ="Please provide a valid quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// The unit price for each individual product
        /// </summary>
        [Required(ErrorMessage ="Please provide a valid price")]
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// The currency type that the payment will be evaluated with
        /// </summary>
        public Currency CurrencyType { get; set; }


        /// <summary>
        /// Determines whether the specific invoice has been paid for or not
        /// </summary>
        public bool Paid { get; set; }

        /// <summary>
        /// Calculates the total cost of the invoice based of the unit price, and the quantity of items ordered
        /// </summary>
        public decimal InvoiceSubTotal()
        {
            return (UnitPrice * Quantity);
        }

        /// <summary>
        /// Calculates the tax that should be applied based off a 10% tax to the subtotal
        /// </summary>
        /// <returns></returns>
        public decimal InvoiceTax()
        {
            return (InvoiceSubTotal() * 0.10m);
        }

        /// <summary>
        /// Calculates the total cost of the invoice, after tax
        /// </summary>
        /// <returns></returns>
        public decimal InvoiceTotal()
        {
            return (InvoiceSubTotal() + InvoiceTax());
        }

        public void UpdateInvoice(Invoice invoice)
        {
            this.ClientName = invoice.ClientName;
            this.ClientAddress = invoice.ClientAddress;
            this.ShipmentDate = invoice.ShipmentDate;
            this.PaymentDueDate = invoice.PaymentDueDate;
            this.ProductName = invoice.ProductName;
            this.Quantity = invoice.Quantity;
            this.UnitPrice = invoice.UnitPrice;
            this.CurrencyType = invoice.CurrencyType;
            this.Paid = invoice.Paid;
        }


        
    }
}
