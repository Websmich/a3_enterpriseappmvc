﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseAppMvc.Domain.Entities;

namespace EnterpriseAppMvc.Domain.Persistance
{
    /// <summary>
    /// The DatbaseUser Repository class will handle all of the data base communication lreating to the users inthe application
    /// </summary>
    public class DatabaseUserRepository : IUserRepository
    {
        private AppDbContext _dbContext;

        public DatabaseUserRepository()
        {
            _dbContext = new AppDbContext();
        }

        /// <summary>
        /// Gets all the users from the database
        /// </summary>
        public IEnumerable<User> UserDatabase
        {
            get
            {
                return _dbContext.Users;
            }
        }

        /// <summary>
        /// When the web application makes a new user, insert them into the database
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(User user)
        {
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Retrieve the details about a given user based on the username supplied
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public User RetrieveUser(string username)
        {
            User user = _dbContext.Users.FirstOrDefault(u => u.Username == username);
            return user;
        }
    }
}
