﻿using EnterpriseAppMvc.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//  Michael Webster
//  Assignment 3 - Enterprise App with MVC
//  PROG30000 - Enterprise Software Systems
//  November 27th 2016 11:59 PM

namespace EnterpriseAppMvc.Domain.Persistance
{
    /// <summary>
    /// The IInvoiceRepository will serve as interface for a mock repository that will have pre-existing invoices for the program
    /// </summary>
    public interface IInvoiceRepository
    {
        IEnumerable<Invoice> InvoiceDatabase { get; }

        void SaveInvoice(Invoice invoice);
    }
}
