﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseAppMvc.Domain.Entities;

namespace EnterpriseAppMvc.Domain.Persistance
{
    public class DatabaseInvoiceRepository : IInvoiceRepository
    {
        private AppDbContext _dbContext;

        public DatabaseInvoiceRepository()
        {
            _dbContext = new AppDbContext();
        }
        /// <summary>
        /// Contacts the database and returns all the invoices
        /// </summary>
        public IEnumerable<Invoice> InvoiceDatabase
        {
            get
            {
                return _dbContext.Invoices;
            }
        }

        /// <summary>
        /// The SaveInvoice method will save the state of an invoice in the table
        /// </summary>
        /// <param name="invoice"></param>
        public void SaveInvoice(Invoice invoice)
        {
            //check to see if the invoice already exists in the table
            if (invoice.InvoiceNumber == 0)
            {
                //Add the invoice to the table
                _dbContext.Invoices.Add(invoice);

            }
            else
            {
                //Find the invoice that already exists 
                Invoice invoiceEntity = _dbContext.Invoices.Find(invoice.InvoiceNumber);

                //change the attributes regarding the invoice
                invoiceEntity.UpdateInvoice(invoice);
            }
            //save changes
            _dbContext.SaveChanges();
        }
    }
}
