﻿using EnterpriseAppMvc.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseAppMvc.Domain.Persistance
{
    /// <summary>
    /// The AppDbContext inherits from the DbContext class for the purpose of using the Entity framewirk to communicate the program and the database
    /// </summary>
    class AppDbContext : DbContext
    {
        public AppDbContext(): base("InvoiceAppDbConnection")
        {

        }

        /// <summary>
        /// Retrieves all the rows in the Invoice table
        /// </summary>
        public DbSet<Invoice> Invoices { get; set; }

        /// <summary>
        /// Rtrieves all the rows in the User table
        /// </summary>
        public DbSet<User> Users { get; set; }
    }
}
