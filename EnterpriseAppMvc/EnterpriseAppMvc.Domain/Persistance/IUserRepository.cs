﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnterpriseAppMvc.Domain.Entities;

using System.Threading.Tasks;

namespace EnterpriseAppMvc.Domain.Persistance
{
    public interface IUserRepository
    {
        IEnumerable<User> UserDatabase { get; }

        void AddUser(User user);

        User RetrieveUser(string username);
    }
}
