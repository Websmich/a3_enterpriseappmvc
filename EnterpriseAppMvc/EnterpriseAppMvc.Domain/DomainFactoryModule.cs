﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseAppMvc.Domain.Persistance;
using Moq;
using EnterpriseAppMvc.Domain.Entities;

namespace EnterpriseAppMvc.Domain
{
    /// <summary>
    /// The DomainFactoryModule class will use the Ninject framework added to the web app to bind different classes with eachother
    /// to achieve looser coupling
    /// </summary>
    public class DomainFactoryModule : NinjectModule
    {
        /// <summary>
        /// The Load() method will handle all of the logic to deal with binding the classes and creating the mock repository
        /// </summary>
        public override void Load()
        {
            //bind the IRegisteredInvoices interface with the RegisteredInvoices class to loose the coupling
            Bind<IRegisteredInvoices>().To<RegisteredInvoices>().InSingletonScope();
            Bind<IRegisteredUsers>().To<RegisteredUsers>().InSingletonScope();

            Bind<IInvoiceRepository>().To<DatabaseInvoiceRepository>();
            Bind<IUserRepository>().To<DatabaseUserRepository>();

        }
    }
}
