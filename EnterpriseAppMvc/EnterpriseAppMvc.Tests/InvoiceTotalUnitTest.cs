﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Moq;
using EnterpriseAppMvc.Domain.Persistance;
using EnterpriseAppMvc.Domain.Entities;
using System.Collections.Generic;
using EnterpriseAppMvc.Domain;

namespace EnterpriseAppMvc.Tests
{

    [TestClass]
    public class InvoiceTotalUnitTest
    {
        private IKernel _diKernal = new StandardKernel();

        /// <summary>
        /// the invoice unit test will test the different price calculations for a single invoice, to make sure the subtotal, tax amount, and total are
        /// being calculated properly
        /// </summary>
        [TestMethod]
        public void InvoiceUnitTest()
        {
            // Step 1. Creating the mock invoice
            Mock<IInvoiceRepository> testMock = new Mock<IInvoiceRepository>();

            testMock.Setup(m => m.InvoiceDatabase).Returns(
                new List<Invoice>
                {
                    new Invoice { ClientName = "Michael W.", ClientAddress = "239 Alley Road.  ", ShipmentDate = new DateTime(2016, 1, 1), PaymentDueDate = new DateTime(2016, 12, 31), ProductName = "Keyboard", Quantity = 3, UnitPrice = 50.50m, CurrencyType = Currency.CAD, InvoiceNumber = 1 },
                }
                );
            _diKernal.Load(new DomainFactoryModule());
            _diKernal.Rebind<IInvoiceRepository>().ToConstant(testMock.Object);

            IRegisteredInvoices invRepo = _diKernal.Get<IRegisteredInvoices>();

            //Step 2. Act

            decimal subtotal = 0;
            decimal taxAmount = 0;
            decimal total = 0;
            foreach (Invoice inv in invRepo.InvoiceList)
            {
                subtotal = inv.InvoiceSubTotal(); //calculate the subtotal
                taxAmount = inv.InvoiceTax(); //calculate the tax that will be applied
                total = inv.InvoiceTotal(); //calculate the total, which is subtotal + tax applied
            }


            //Step 3 Assert

            Assert.IsTrue(subtotal == 151.50m); //the subtotal must be equal to 3 * 50.5 which is 151.5. 

            /*
             * After conducting the unit test, the subtotal was being calculated properly
             */

            Assert.IsTrue(taxAmount == 15.15m); //the tax amount applied must be equal to subtotal (151.50 in this case) * 0.10 (10%) which is 15.15

            /*
             * After conducting the unit test, the tax applied is being calculated properly
             */

            Assert.IsTrue(total == 166.65m); //the total invoice should be tax amount (15.15) + subtotal (151.50) which is 166.65

            /*
             * After conducting the unit test, total amount is being added up properly
             */
        }

        /// <summary>
        /// The ReceivablesUnitTest test method will make sure that when invoices are being marked as paid, they are no longer being included when 
        /// the program retrieves only unpaid invoices
        /// </summary>
        [TestMethod]
        public void ReceivablesUnitTest()
        {
            //Step 1. Create the mock repo with 3 different invoices
            Mock<IInvoiceRepository> testMock = new Mock<IInvoiceRepository>();

            testMock.Setup(m => m.InvoiceDatabase).Returns(
                new List<Invoice>
                {
                    new Invoice { ClientName = "Client 1 - Not Paid For", ClientAddress = "Address 1", ShipmentDate = new DateTime(2016, 1, 1), PaymentDueDate = new DateTime(2016, 12, 31), ProductName = "Item 1", Quantity = 3, UnitPrice = 50.50m, CurrencyType = Currency.CAD, InvoiceNumber = 1 },
                    new Invoice { ClientName = "Client 2 - Paid For", ClientAddress = "Address 2", ShipmentDate = new DateTime(2016, 1, 1), PaymentDueDate = new DateTime(2016, 12, 31), ProductName = "Item 2", Quantity = 5, UnitPrice = 10.50m, CurrencyType = Currency.CAD, InvoiceNumber = 2, Paid = true },
                    new Invoice { ClientName = "Client 3 - Not Paid For", ClientAddress = "Address 3", ShipmentDate = new DateTime(2016, 1, 1), PaymentDueDate = new DateTime(2016, 12, 31), ProductName = "Item 3", Quantity = 2, UnitPrice = 25.25m, CurrencyType = Currency.CAD, InvoiceNumber = 3 }
                }
                );
            _diKernal.Load(new DomainFactoryModule());
            _diKernal.Rebind<IInvoiceRepository>().ToConstant(testMock.Object);

            IRegisteredInvoices invRepo = _diKernal.Get<IRegisteredInvoices>();

            //Step 2. Act

            IEnumerable<Invoice> outstandingInvoices = invRepo.FindInvoice(inv => inv.Paid == false); //only selects the items from the list where the invoice is unpaid (isPaidFor == false)
            
            decimal totalOne = 0;
            decimal totalTwo = 0;

            foreach(Invoice inv in outstandingInvoices)
            {
                if (!inv.Paid)//only add invoice to the total outstanding if it is NOT paid for
                {
                    totalOne += inv.InvoiceTotal();
                }
            }

            invRepo.AddInvoice(new Invoice { ClientName = "Client 4 - Not Paid For & newly added", ClientAddress = "Address 4", ShipmentDate = new DateTime(2016, 1, 1), PaymentDueDate = new DateTime(2016, 12, 31), ProductName = "Item 4", Quantity = 1, UnitPrice = 10.00m, CurrencyType = Currency.CAD, InvoiceNumber = 4 });
            outstandingInvoices = invRepo.FindInvoice(inv => inv.Paid == false); //reselect all the unpaid invoices, but make sure the newly unpaid for one is included

            foreach (Invoice inv in outstandingInvoices)
            {
                if (!inv.Paid)
                {
                    totalTwo += inv.InvoiceTotal();
                }
            }

            //Step 3. Assert
            Assert.IsTrue(totalOne == 222.2m); //tests to make sure only unpaid invoices are added up
            /*
             * After conducting the unit test, the findInvoice filter method is being successfully used to only obtain unpaid invoices
             */
            Assert.IsTrue(totalTwo == 233.2m); //tests to make sure the newly unpaid for invoice is being accounted for
            /*
             * After conducting the unit test, the findInvoice filter method only obtaining unpaid invoices also accounts for newly added unpaid invoices
             */

        }
    }
}
