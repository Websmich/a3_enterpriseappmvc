﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnterpriseAppMvc.Domain.Entities;

//  Michael Webster
//  Assignment 3 - Enterprise App with MVC
//  PROG30000 - Enterprise Software Systems
//  November 27th 2016 11:59 PM

namespace EnterpriseAppMvc.Controllers
{
    /// <summary>
    /// The InvoiceController will handle all of the logic relating to invoices and the views that associate with them
    /// </summary>
    public class InvoiceController : Controller
    {
        /// <summary>
        /// The USER_TYPE_NAME is a constant string which will serve as the name of the session object to hold the information
        /// about which type of user is currently signed in (admin or normal)
        /// </summary>
        private const string USER_ACCOUNT = "UserAccount";

        /// <summary>
        /// The USER_NAME const variable will serve as the name of the session object to hold the string of the username of the user using
        /// the web app
        /// </summary>
        private const string USER_NAME = "UserName";

        /// <summary>
        /// This constant variable will be the string of the name of the session which will keep track of the invoice list 
        /// </summary>
        private const string INVOICE_REGISTER_SESSION_NAME = "Invoices";

        
        private IRegisteredInvoices _registeredInvoices;

        /// <summary>
        /// The constructor will initialize the registered invoices class to allow access to its methods
        /// </summary>
        public InvoiceController(IRegisteredInvoices regInv)
        {

            _registeredInvoices = regInv;
        }
        /// <summary>
        /// The InvoiceRegister class will hold the information relating the the invoices registered in the web application. It will
        /// store it in a session variable so it may be used throughout the entirety of the state of the web application
        /// </summary>
        public IRegisteredInvoices InvoiceRegister
        {
            get
            {    
                if (Session[INVOICE_REGISTER_SESSION_NAME] == null) //if the session does not exist, make it equal the newly created RegisteredInvoice object
                {
                    Session[INVOICE_REGISTER_SESSION_NAME] = _registeredInvoices;
                }
                else //the session exists, so it may reused
                {
                    _registeredInvoices = Session[INVOICE_REGISTER_SESSION_NAME] as IRegisteredInvoices;
                }
                return _registeredInvoices;
            }
        }


        /// <summary>
        /// The home ActionResult method will return the home page found in the Invoice section of the web application. From there,
        /// The user can which action they want to perform (such as viewing invoices, editing them, etc)
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Home()
        {
            return View();
        }

        /// <summary>
        /// The AddInvoice ActionResult method will return a view which will allow the user to create a new invoice
        /// to be added to the list of saved invoices
        /// </summary>
        /// <returns></returns>
        public ActionResult InvoiceEntry()
        {
            ViewBag.title = "Add Invoice";
            ViewBag.submitBtn = "Submit Invoice";
            return View();
        }

        /// <summary>
        /// The AddInvoice ActionResult HttpPost method will receieve the information the user entered on the form and make
        /// a new invoice. It will also verify that all of the information is entered and correct (i.e. unit price is only a decimial,
        /// quantity is an int, etc). If the invoice is being editted, it will be updated.
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="shipDate"></param>
        /// <param name="payDate"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InvoiceEntry(Invoice invoice)
        {
            
            //check to see if all the information has been added properly
            if (ModelState.IsValid)
            {
                InvoiceRegister.AddInvoice(invoice);
                ViewBag.message = "The invoice was added successfully!";
                return View("Home");
            }
            //the information was not entered correctly. Return the view to the user
            else
            {
                ViewBag.submitBtn = "Add Invoice";
                if (invoice.InvoiceNumber != 0) //if the invoice already existed, make the reviewing state of the page true again
                {
                    ViewBag.isReviewing = true;
                    ViewBag.submitBtn = "Edit Invoice";
                }
                return View(InvoiceRegister.InvoiceList.FirstOrDefault(inv => inv.InvoiceNumber == invoice.InvoiceNumber));//make sure the invoice being editted before is being returned
            }
        }


        /// <summary>
        /// The ViewInvoices ActionResult method will return view that will display a list of the current invoices available
        /// to the admin viewing it
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewInvoices(string message)
        {

            ViewBag.message = message;
            return View(_registeredInvoices.FindInvoice(inv => inv.Paid == false));
        }

        /// <summary>
        /// The manageInvoice ActionResult method will return the same view that was used to display the ability to 
        /// add invoices
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageInvoice(string trackingNum)
        {
            int trackingNumber = int.Parse(trackingNum);
            Invoice invoice = InvoiceRegister.InvoiceList.FirstOrDefault(i => i.InvoiceNumber == trackingNumber);
            
            //set viewbag values to allow the page to display as an editable invoice
            ViewBag.isReviewing = true;
            ViewBag.title = "Edit Invoice";
            ViewBag.submitBtn = "Edit Invoice";
            return View("InvoiceEntry", invoice);
        }
        /// <summary>
        /// method which will log the user out and end the session
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "UserAccount");

        }
    }
}