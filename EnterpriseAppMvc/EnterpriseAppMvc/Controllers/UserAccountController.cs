﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnterpriseAppMvc.Domain.Entities;

//  Michael Webster
//  Assignment 3 - Enterprise App with MVC
//  PROG30000 - Enterprise Software Systems
//  November 27th 2016 11:59 PM

namespace EnterpriseAppMvc.Controllers
{
    /// <summary>
    /// The UserController will hold all the logic relating to the user and the invoices relating to them
    /// </summary>
    public class UserAccountController : Controller
    {

        /// <summary>
        /// The USER_TYPE_NAME is a constant string which will serve as the name of the session object to hold the information
        /// about which type of user is currently signed in (admin or normal)
        /// </summary>
        private const string USER_ACCOUNT = "UserAccount";


        private IRegisteredUsers _registeredUsers;

        public UserAccountController(IRegisteredUsers reg)
        {
            _registeredUsers = reg;
        }

        /// <summary>
        /// The main entry point of the web application. It will contain a login page for the user to enter their
        /// credentials on
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        /// <summary>
        /// The login method will validate if the user has correctly entered a password or not
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                ViewBag.message = "Please provide a valid username and password";
                return View("Index");
            }
            //validate the user
            User user = _registeredUsers.ValidateUser(username, password);

            //check to see if the user authenticated properly by indentifying if a null value was returned
            if (user.UserID != 0)
            {
                //set the session
                Session[USER_ACCOUNT] = user;
                //allow the user to continue 
                return RedirectToAction("Home", "Invoice");
            }
            else
            {
                //return the user back to the home page notifying them that their user name or password was incorrect
                ViewBag.message = "Invalid username or password. Please try again!";
                return View("Index");
            }

        }


        /// <summary>
        /// The GET method for the CreateAccount
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateAccount()
        {
            
            return View();
        }

        /// <summary>
        /// Post method for the CreateAccount. It will make sure the two passwords which have been supplied to the user match,
        /// then it will send off the newly created user to the rest of the program to prepare for password hashing and insertion into the database
        /// </summary>
        /// <param name="user"></param>
        /// <param name="passwordTwo"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateAccount(User user, string passwordTwo)
        {

            //Check to see everything entered was correct
            if (ModelState.IsValid)
            {
                //Check to see if the password confirmation and the normal password are the sme
                if (user.Password == passwordTwo)
                {
                    //Send the user off to be inserted into the database
                    _registeredUsers.AddUser(user);
                    return RedirectToAction("Index");
                }
                else
                {
                    //supply an error to the user back on the create account screen
                    ViewBag.error = "Please provide matching passwords in the fields.";
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}